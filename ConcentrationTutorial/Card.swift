//
//  Card.swift
//  ConcentrationTutorial
//
//  Created by NFlahavan on 8/28/18.
//  Copyright © 2018 NFlahavan. All rights reserved.
//

import Foundation

struct Card: Hashable {
    private var identifier: Int
    var isFaceUp = false
    var isMatched = false
    var hashValue: Int { return identifier }
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
}
