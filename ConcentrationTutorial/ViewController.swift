//
//  ViewController.swift
//  ConcentrationTutorial
//
//  Created by NFlahavan on 8/28/18.
//  Copyright © 2018 NFlahavan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private(set) lazy var game = Concentration(numberOfPairsOfCards: numPairsOfCards)
    var numPairsOfCards: Int {
        return (cards.count + 1) / 2
    }
    @IBOutlet weak var FlipsContainer: UIView!{
        didSet {
            FlipsContainer.layer.borderWidth = CGFloat(0.5)
            FlipsContainer.layer.borderColor = UIColor.orange.cgColor
            FlipsContainer.layer.cornerRadius = CGFloat(5.0)
        }
    }
    @IBOutlet private weak var flipsLabel: UILabel! {
        didSet {
            updateFlipCountLabel()
        }
    }
    @IBOutlet private var cards: [UIButton]! {
        didSet {
            for card in cards {
                card.isOpaque = false
                card.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1).withAlphaComponent(0.7)
            }
        }
    }
    private(set) var flipCount = 0 {
        didSet {
            updateFlipCountLabel()
        }
    }
    
    private func updateFlipCountLabel() {
        let attributes: [NSAttributedStringKey:Any] = [
            .strokeWidth : 3.0,
            .strokeColor : UIColor.orange
        ]
        let attribString = NSAttributedString(string: "flips: \(flipCount)", attributes: attributes)
        flipsLabel.attributedText = attribString
    }
    
    @IBAction private func cardTouched(_ sender: UIButton) {
        flipCount += 1
        let cardNumber = cards.index(of: sender)!
        game.chooseCard(at: cardNumber)
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        for index in cards.indices {
            let button = cards[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControlState.normal)
                button.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControlState.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1).withAlphaComponent(0.7)
            }
        }
    }
    
    private let emojiChoices = "👻🎃👽💀👹🤡🤠🤖"
    private var usableEmojis = "👻🎃👽💀👹🤡🤠🤖"
    
    private var emoji = [Card:String]()
    
    private func emoji (for card: Card) -> String{
        if emoji[card] == nil, usableEmojis.count > 0 {
            let randomStringIndex = usableEmojis.index(usableEmojis.startIndex, offsetBy: usableEmojis.count.arc4random)
            emoji[card] = String(usableEmojis.remove(at: randomStringIndex))
        }
        return emoji[card] ?? "?"
    }
}
